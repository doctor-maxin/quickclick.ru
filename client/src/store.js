import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null
  },
  getters: {
    user: state => {
      return state.user
    }
  },
  mutations: {
    ADD_USER (state, userData) {
      state.user = userData
    }
  },
  actions: {
    ADD_USER: (context, payload) => {
      return new Promise((resolve) => {
        context.commit('ADD_USER', payload)
        resolve()
      })
    },
    ADD_USER_BYID: (context, id) => {
      return new Promise((resolve) => {
        axios.get(`/api/users/${id}`)
          .then(res => {
            context.commit('ADD_USER', res.data)
            resolve()
          })
      })
    },
  }
})
