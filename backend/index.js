const Express = require('express'),
    app = Express(),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    config = require('./config/config'),
    db = require('./db')

app.use(morgan(':method :url :status'))
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())

app.get('/api', (req, res) => {
    res.send('is api v1')
})

app.get('/api/users', async (req, res) => {
    const connection = await db.getConnection();
    const users = await connection.executeQuery(`Select * from users`)
    res.send(users);
})
app.get('/api/users/:id', async (req, res) => {
    const connection = await db.getConnection();
    const users = await connection.executeQuery(`Select * from users WHERE id = ${req.params.id}`)
        
   	res.json(users[0])   
})
app.post('/api/auth', async (req, res) => {
    let rst = false;
    const connection = await db.getConnection();
    const users = await connection.executeQuery(`Select * from users`)
    for (usr of users) {
        if (req.body.email == usr.email && req.body.password == usr.password) {
            rst = true
            res.json(usr)
            break;
        }
    }
    if (!rst) {
        res.status(401)
    }
})


app.listen(process.env.PORT || config.port, () => {
    console.log(`Server start on port ${process.env.PORT || config.port}`)
})
