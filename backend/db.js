const mysql = require('mysql'),
    BaseDatabase = require('mysql-async-wrapper').default,
    config = require('./config/config')

const pool = mysql.createPool(config.db), db = new BaseDatabase(pool)
module.exports = db


