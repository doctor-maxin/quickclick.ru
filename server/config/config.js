const store = require('koa-session-local')

module.exports = {
    port: 8081,
    session: {
        key: 'rim porta;',
        masAge: 8640000,
        autoCommit: true,
        overwrite: false,
        httpOnly: true,
        signed: true,
        rolling: true,
        renew: false, 
        store: new store()
    }
}