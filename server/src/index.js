const Koa = require('koa'),
    app = new Koa(),
    router = require('./router').router,
    logger = require('koa-logger'),
    koaBody = require('koa-body'),
    cors = require('koa-cors'),
    config = require('./../config/config'),
    static = require('koa-static'),
    session = require('koa-session')

app.keys = ['rim portal']
app.use(session(config.session, app))
app.use(static(__dirname + '/../../client/dist/'))
app.use(cors())
app.use(koaBody())
app.use(logger())
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(process.env.PORT || config.port, () => {
    console.log(`Server start on port ${process.env.PORT || config.port}`)
})