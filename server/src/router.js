const Router = require('koa-router'),
    router = new Router(),
    fs = require('fs'),
    db = require('./db'),
    koaBody = require('koa-body')



let readFileThunk = src => {
    return new Promise((resolve, reject) => {
        fs.readFile(src, {'encoding': 'utf8'}, (err, data) => {
            if (err) return reject(err)
            resolve(data)
        })
    })
}

router 
    .get('/', async (ctx, next) => {
        fs.readdir('./../../test-pr/dist/', (err, files) => {
            console.log(files)
        })
        ctx.body = await readFileThunk('./../../test-pr/dist/index.html')
    })
    .get('/api/users', async (ctx, next) => {
        const connection = await db.getConnection();
        const users = await connection.executeQuery(`Select * from users`)
        ctx.body = users;
    })
    .get('/api/me', async (ctx, next) => {
        console.log(ctx.session)

        if (ctx.session.auth == true) {
            ctx.body = ctx.session.store.user
        } else ctx.status = 401
    })
    .post('/api/auth', async (ctx, next) => {
        let rst = false;
        const connection = await db.getConnection();
        const users = await connection.executeQuery(`Select * from users`)
        // ctx.request.body = JSON.parse(ctx.request.body)
        for (usr of users) {
            if (ctx.request.body.email == usr.email &&
                ctx.request.body.password == usr.password) {
                    rst = true
                    console.log(ctx.session.ContextStore)
                    ctx.session.ContextStore.auth = true
                    // ctx.session.store.user = usr
                    ctx.session.save();
                }
        }
        if (rst) {
            ctx.status = 200;
        } else ctx.status = 401
    })
module.exports.router = router;